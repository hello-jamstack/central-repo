#### Todo
- [ ] Journey doc
- [ ] GitLab CI
- [ ] app insights
- [ ] logout timeout in web app
- [ ] functions as npm lib
- [ ] custom host name
- [ ] specific CORS

#### Done
- [X] Java SDK
- [X] .NET SDK
- [X] Javascript SDK
- [X] CLI exception handling
- [X] secure function token
- [X] CLI
- [X] dynamic AD application
- [X] handle no user
- [X] fix redir fn
- [X] fix DB in delete fn
- [X] initialize npm in functions
- [X] automate cors
- [X] automate always on
- [X] test data in terraform
- [X] dynamic function IDs
- [X] Terraform functions
- [X] Terraform db
- [X] test data script
