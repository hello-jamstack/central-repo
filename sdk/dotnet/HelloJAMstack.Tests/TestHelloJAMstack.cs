using System;
using Xunit;
using HelloJAMstack;

namespace HelloJAMstack.Tests
{
    public class TestHelloJAMstack
    {
        [Fact]
        public async void TestFetchMessage()
        {
            HelloJAMstack hjs = new HelloJAMstack();
            var secret = System.Environment.GetEnvironmentVariable("HELLO_JAMSTACK_SECRET");
            if (secret == null) {
              throw new Exception("Please set your environment variable HELLO_JAMSTACK_SECRET with your Hello JAMstack secret your admin gave you");
              
            }
            var message = await hjs.fetchMessageWithSecret("https://hellovuepress-webapp.azurewebsites.net/config-hello-jamstack.json",secret);
            Assert.Equal("It is no secret. You are awesome!", message);
        }
    }
}
