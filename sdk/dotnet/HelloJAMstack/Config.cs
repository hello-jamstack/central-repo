namespace HelloJAMstack
{
    public class Config
    {
        public string Tenant { get; set; }
        public string ClientId { get; set; }
        public string CacheLocation { get; set; }
        public string RedirectUri { get; set; }
        public string ApiEndpoint { get; set; }
    }
}