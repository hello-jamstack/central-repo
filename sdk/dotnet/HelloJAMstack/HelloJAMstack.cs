﻿using System;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;

namespace HelloJAMstack
{
    /// <summary>
    /// Library class you can use to call the Hello JAMstack online API.
    /// </summary>
    public class HelloJAMstack
    {
        /// <summary>
        /// Fetches the Hello JAMstack secret message with an authorization secret. Your adminstrator will
        /// give you the configUrl and secret.
        /// </summary>
        /// <param name="configUrl">The url where the JSON object is that configures the message request.</param>
        /// <param name="secret">The authorization secret for the Hello JAMstack system.</param>
        /// <returns>The generated Markdown content.</returns>
        public async Task<string> fetchMessageWithSecret(string configUrl, string secret) 
        {

            string rval = null;

            try
            {
              var Config = await configUrl
                  .WithHeader("accept", "application/json")
                  .GetJsonAsync<Config>();

              var TokenResponse = await "https://login.microsoftonline.com/"
                  .AppendPathSegment(String.Format("{0}/oauth2/token", Config.Tenant))
                  .WithHeader("Accept", "application/json")
                  .WithHeader("content-type", "application/x-www-form-urlencoded")
                  .PostStringAsync(String.Format("grant_type=client_credentials&client_id={0}&client_secret={1}", Config.ClientId, secret))
                  .ReceiveJson<TokenResponse>();

              rval = await this.fetchMessageWithToken(configUrl,TokenResponse.access_token);
            }
            catch (Exception ex)
            {
              Console.Write(ex);
              throw ex;
            }

            return rval; 
        }

        /// <summary>
        /// Fetches the Hello JAMstack secret message with an bearer token. Your adminstrator will
        /// give you the configUrl. You have the get the bearer token either through a user login
        /// or if you are doing a system to system call use fetchMessageWithSecret and it will 
        //// handle the bearer token for you.
        /// </summary>
        /// <param name="configUrl">The url where the JSON object is that configures the message request.</param>
        /// <param name="toekn">The OAuth2 bearer token that you go by logging into the JAMstack system.</param>
        /// <returns>The generated Markdown content.</returns>
        public async Task<string> fetchMessageWithToken(string configUrl, string Token) 
        {
            string rval = null;

            try
            {
              var Config = await configUrl
                  .WithHeader("Accept", "application/json")
                  .GetJsonAsync<Config>();

              var Message = await Config.ApiEndpoint
                  .AppendPathSegment("hello")
                  .WithHeader("accept", "application/json")
                  .WithOAuthBearerToken(Token)
                  .GetJsonAsync<Message>();

              rval = Message.message;

            }
            catch (Exception ex)
            {
              Console.Write(ex);
              throw ex;
            }

            return rval;
        }
    }
}
