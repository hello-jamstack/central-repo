package com.flacito.jamstack;

import org.junit.Test;
import static org.junit.Assert.*;

public class HelloJAMstackTest {
  @Test public void testFetchMessageWithSecret() {
    HelloJAMstack hjs = new HelloJAMstack();
    assertEquals("It is no secret. You are awesome!", hjs.fetchMessageWithSecret(System.getenv("HELLO_JAMSTACK_CONFIG_URL"),System.getenv("HELLO_JAMSTACK_SECRET")));
  }
}
