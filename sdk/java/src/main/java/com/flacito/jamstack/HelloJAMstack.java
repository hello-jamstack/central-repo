package com.flacito.jamstack;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.http.JsonNode;

/**
 * HelloJAMStack
 * Provides methods to fetch the secret message.
 * @author Brian T. Webb
 * @version 0.1.0
 * @since 0.0.1
 */
public class HelloJAMstack {

  /**
   * fetchMessageWithSecret
   * 
   * Fetch the secret message with a shared authentication secret.
   * Use this for process to process communication, like when
   * implementing a CLI or similar.
   * 
   * @param configUrl - url that links to the config JSON file for the Hello
   *  JAMstack service.  Your administrator will give you this.
   * @param secret - shared login secret, this rotates often and can be
   *  obtained from your administrator.
   * @return secret message string
   */
  public String fetchMessageWithSecret(String configUrl, String secret) {

    String rval = null;

    try {
      HttpResponse<JsonNode> jsonResponse = Unirest.get(configUrl)
        .header("accept", "application/json")
        .asJson();
    
      String subscriptionId = jsonResponse.getBody().getObject().getString("tenant");
      String clientId = jsonResponse.getBody().getObject().getString("clientId");

      jsonResponse = Unirest.post("https://login.microsoftonline.com/" + subscriptionId + "/oauth2/token")
        .header("content-type", "application/x-www-form-urlencoded")
        .header("accept", "application/json")
        .body("grant_type=client_credentials&client_id=" + clientId + "&client_secret=" + secret).asJson();

      String token = jsonResponse.getBody().getObject().getString("access_token");

      rval = this.fetchMessageWithToken(configUrl, token);
    } catch (UnirestException e) {
      e.printStackTrace();
      throw (new RuntimeException(e));
	}

    return rval;
  }

  /**
   * fetchMessageWithToken
   * 
   * Fetch the secret message with a bearer token from a user login.
   * Use this for supporting user interfaces, like a web or mobile app.
   * 
   * @param configUrl - url that links to the config JSON file for the Hello
   *  JAMstack service.  Your administrator will give you this.
   * @param token - OAuth2 bearer token.
   * @return secret message string
   */
  public String fetchMessageWithToken(String configUrl, String token) {
    String rval = null;

    try {
      HttpResponse<JsonNode> jsonResponse = Unirest.get(configUrl)
      .header("accept", "application/json")
      .asJson();

      String apiEndpoint = jsonResponse.getBody().getObject().getString("apiEndpoint");

      jsonResponse = Unirest.get(apiEndpoint+"/hello")
        .header("accept", "application/json")
        .header("authorization", "Bearer " + token)
        .asJson();

      rval = jsonResponse.getBody().getObject().getString("message");

    } catch (UnirestException e) {
      e.printStackTrace();
      throw (new RuntimeException(e));
    }

    return rval;
  }
}
