
import HelloJAMstack from '../src/hello-jamstack';

function statusCallback(message) {
  console.log(message);
}

describe('Hello JAMstack test suite', () => {

  let helloJAMstack = new HelloJAMstack();

  it('fetches the message with a secret', async () => {
    let response = await helloJAMstack.fetchMessageWithSecret('https://fakeserver.com/config.json', 'mocksecret', statusCallback);
    expect(response.message).toEqual('It is no secret. You are awesome!');
  });

  it('fetches the message with a bearerToken', async () => {
    let response = await helloJAMstack.fetchMessageWithToken('https://fakeserver.com/config.json', 'mock.bearer.token', statusCallback);
    expect(response.message).toEqual('It is no secret. You are awesome!');
  });

});
