# hello-jamstack-sdk

SDK for interacting with the Hello JAMstack API.

## Install

1. `npm install hello-jamstack-sdk` or `yarn add hello-jamstack-sdk`
2. Import the classes where you need them.
