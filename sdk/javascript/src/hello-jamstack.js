import axios from 'axios';

/**
 * @class HelloJAMStack
 * Provides methods to fetch the secret message.
 * @author Brian T. Webb
 * @version 0.1.0
 * @since 0.0.1
 * @license MIT
 *
 * @see https://github.com/flacito/hellojamstack/sdk/javascript hellojamstack
 */
export default class HelloJAMstack {

  /**
   * @function fetchMessageWithSecret
   * Fetch the secret message with a shared authentication secret.
   * Use this for process to process communication, like when
   * implementing a CLI or similar.
   * @param {string} configUri - url that links to the config JSON file for the Hello
   *  JAMstack service.  Your administrator will give you this.
   * @param {string} secret - shared login secret, this rotates often and can be
   *  obtained from your administrator.
   * @param {function} statusCallback - function that will be called with period
   *  status messages, this function should support a single status string argument
   * @see http://localhost:8080/api.html#token-from-secret shared secret
   */
  fetchMessageWithSecret(configUri, secret, statusCallback) {
    return new Promise((resolve, reject) => {
      statusCallback('fetching client identification');
      axios
        .get(configUri, {
          headers: {
            'accept': 'application/json'
          }
        })
        .then(response => {
          let clientId = response.data.clientId;
          let subscriptionId = response.data.subscriptionId;
          
          statusCallback('authenticating using configured secret');
          axios
            .get(`https://login.microsoftonline.com/${subscriptionId}/oauth2/token`,
              `grant_type=client_credentials&client_id=${clientId}&client_secret=${secret}`,
              { headers: {
                'content-type': 'application/x-www-form-urlencoded',
                'accept': 'application/json'
              }
              })
            .then(response => {
              let bearerToken = response.data.access_token;

              this.fetchMessageWithToken(configUri, bearerToken, statusCallback)
                .then(response => {
                  let message = response;

                  statusCallback('secret message fetch process complete');
                  resolve(message);
                });
            });
        });
    });
  }

  /**
   * @function fetchMessageWithToken
   * Fetch the secret message with a bearer token from a user login.
   * Use this for supporting user interfaces, like a web or mobile app.
   * @param {string} configUri - url that links to the config JSON file for the Hello
   *  JAMstack service.  Your administrator will give you this.
   * @param {string} author - OAuth2 bearer token.
   * @param {function} statusCallback - function that will be called with period
   *  status messages, this function should support a single status string argument
   * @see http://localhost:8080/api.html#response-2 bearer token
   */
  fetchMessageWithToken(configUri, bearerToken, statusCallback) {
    return new Promise((resolve, reject) => {
      statusCallback('fetching client identification');
      axios
        .get(configUri, {
          headers: {
            'accept': 'application/json'
          }
        })
        .then(response => {
          let apiEndpoint = response.data.apiEndpoint;

          statusCallback('fetching the secret message');
          axios
            .get(`${apiEndpoint}/hello`,
              {
                headers: {
                  'accept': 'application/json',
                  'authorization': `Bearer ${bearerToken}`
                }
              })
            .then(response => {
              let message = response.data;

              statusCallback('secret message fetch process complete');
              resolve(message);
            });

        });
    });
  }
}
