export default {
  input: 'src/index.js',
  output: {
    file: 'lib/hello-jamstack-sdk.js',
    format: 'cjs'
  }
};
