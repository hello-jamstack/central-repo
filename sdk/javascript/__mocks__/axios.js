const log = console.log
const mockApiEndpoint = 'https://fakeserver.com/api';

export default  {
  get: function (url) {
    // log(`this is a mock of axios.get.  url=${url}`);
    if (url === 'https://fakeserver.com/config.json') {
      return Promise.resolve({ 
        data: {
          clientId: 'fakeclientid',
          subscriptionId: 'fakesubid',
          apiEndpoint: mockApiEndpoint
        }
      });
    }
    else if (url.startsWith('https://login.microsoftonline.com/')) {
      return Promise.resolve({ 
        data: {
          access_token: 'abc123.IamaFAKE.bearertoken'
        } 
      });
    }
    else if (url.startsWith(mockApiEndpoint)) {
      return new Promise((resolve) => { 
        resolve({
          data: {
            message: 'It is no secret. You are awesome!'
          }
        });
      });
    }
  }
}