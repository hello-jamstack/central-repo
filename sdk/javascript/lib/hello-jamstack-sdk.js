'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var axios = _interopDefault(require('axios'));

class HelloJAMstack {

  fetchMessageWithSecret(configUri, secret, statusCallback) {
    return new Promise((resolve, reject) => {
      statusCallback('fetching client identification');
      axios
        .get(configUri, {
          headers: {
            'accept': 'application/json'
          }
        })
        .then(response => {
          let clientId = response.data.clientId;
          let subscriptionId = response.data.subscriptionId;
          let apiEndpoint = response.data.apiEndpoint;

          statusCallback('authenticating using configured secret');
          axios
            .get(`https://login.microsoftonline.com/${subscriptionId}/oauth2/token`,
              `grant_type=client_credentials&client_id=${clientId}&client_secret=${secret}`,
              { headers: {
                'content-type': 'application/x-www-form-urlencoded',
                'accept': 'application/json'
              }
              })
            .then(response => {
              let bearerToken = response.data.access_token;

              this.fetchMessageWithToken(configUri, bearerToken, statusCallback)
                .then(response => {
                  let message = response;

                  statusCallback('secret message fetch process complete');
                  resolve(message);
                });
            });
        });
    });
  }

  fetchMessageWithToken(configUri, bearerToken, statusCallback) {
    return new Promise((resolve, reject) => {
      statusCallback('fetching client identification');
      axios
        .get(configUri, {
          headers: {
            'accept': 'application/json'
          }
        })
        .then(response => {
          let apiEndpoint = response.data.apiEndpoint;

          statusCallback('fetching the secret message');
          axios
            .get(`${apiEndpoint}/hello`,
              {
                headers: {
                  'accept': 'application/json',
                  'authorization': `Bearer ${bearerToken}`
                }
              })
            .then(response => {
              let message = response.data;

              statusCallback('secret message fetch process complete');
              resolve(message);
            });

        });
    });
  }
}

exports.HelloJAMstack = HelloJAMstack;
