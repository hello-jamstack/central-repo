'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var chalk = _interopDefault(require('chalk'));
var fs = _interopDefault(require('fs'));
var os = _interopDefault(require('os'));
var helloJamstackSdk = require('hello-jamstack-sdk');
var ora = _interopDefault(require('ora'));
var clear = _interopDefault(require('clear'));
var figlet = _interopDefault(require('figlet'));
var minimist = _interopDefault(require('minimist'));

class LoginUtils {

  constructor() {
    this.fname = os.homedir() + '/.hav';
  }

  getFileName() {
    return this.fname;
  }

  readSavedLogin() {
    let savedLogin = null;

    if (fs.existsSync(this.fname)) {
      let data = fs.readFileSync(this.fname, {encoding: 'utf-8'});
      savedLogin = JSON.parse(data);
    }

    return savedLogin;
  }

  writeSavedLogin(savedLogin) {
    fs.writeFileSync(this.fname, JSON.stringify(savedLogin, null, ' ')); 
    fs.chmodSync(this.fname,'0600');
    
    return console.log(
      chalk.yellowBright(
        `Login persisted to ${this.fname}`
      )
    );
  }
}

const log$1 = console.log;

class Hello {

  async execute(args) {
    const quiet = args.quiet || args.q;

    let spinner = null;
    if (!quiet) spinner = ora('starting fetch secret message process', {spinner:'simpleDotsScrolling'}).start();
  
    var secret = args.secret || args.s;
    if (secret == null) {
  
      let loginUtils = new LoginUtils();
      let savedLogin = loginUtils.readSavedLogin();
      if (savedLogin != null) {
        secret = savedLogin.secret;
      }
  
      if (secret == null) {
        let errMsg = `"-secret" argument is required or saved login is required. Run "hav help hello" or "have help login" for details.`;
        console.log(
          chalk.red(
            errMsg
          )
        );
        throw(errMsg);
      }
    }

    const configUrl = args["config-url"] || args.c;
    if (configUrl == null) {
      let errMsg = `"--config-url" argument is required. Run "hav help hello" for details.`;
      console.log(
        chalk.red(
          errMsg
        )
      );
      throw(errMsg);
    }
      
    let hjs = new helloJamstackSdk.HelloJAMstack();
    let message = await hjs.fetchMessageWithSecret(configUrl, secret, (status) => {
      if (!quiet) spinner.text = status;
    });

    if (!quiet) spinner.text = 'secret message fetch process complete';
    if (!quiet) spinner.succeed();
    const pretty = args.pretty || args.p;
    pretty ? console.log(JSON.stringify(message, null, ' ')) : console.log(JSON.stringify(message));
    if (!quiet) log$1(chalk.greenBright('complete'));
      
  }

}

const menus = {
  main: {
    command: 'hav [command] <options>',
    help: `
    The Hello VuePress CLI performs the following operations
    against the Hello VuePress online API.

      login .............. logs the user into Hello VuePress
                           with an option to save the login

      logout ............. logs the user out of Hello VuePress

      hello ............... shows the secret hello message

      version ............ show Hello VuePress command line version

      help ............... show help menu for a command

    Options available for all commands:

      --quiet, -q ..... just print the output with no interactive status messages

      --pretty, -p ..... print output in pretty format`,

    args: ' '
    },

  login: {
    command: `hav login [args]`,
    help:`
    logs in the user to Hello VuePress`,
    args: `
      --secret, -s ............ required. shared login secret, this rotates
                                often and can be obtained from your
                                Hello VuePress administrator`
                                
  },

  logout: {
    command: 'hav logout',
    help:`
    logs out the user from Hello VuePress, removing any
    saved login`,
  },

  hello: {
    command: 'hav hello',
    help:`
    shows the secret hello message`,
    args: `
      --config-url, -c ........ required. url that links to the config JSON file
                                for the Hello JAMstack service.  Your
                                administrator will give you this.

      --secret, -s ............ optional. shared login secret, this rotates
                                often and can be obtained from your
                                Hello VuePress administrator. You can optionally
                                use "hav login" to store the secret for use with
                                multiple commands.`
  }

};

class Help {

  execute(args) {
    const subCmd = args._[0] === 'help'
      ? args._[1]
      : args._[0];

    clear();
    console.log(
      chalk.cyan(
        figlet.textSync('Hello VuePress CLI', { font: 'standard', horizontalLayout: 'default' })
      )
    );

    if (subCmd && !menus[subCmd]) {
      console.log(
        chalk.red(
          `no help for command "${subCmd}"`
        )
      );
      console.log();
    }

    console.log(
      chalk.whiteBright(
        menus[subCmd] ? menus[subCmd].command : menus.main.command
      )
    );

    console.log(
      chalk.yellowBright(
        menus[subCmd] ? menus[subCmd].help : menus.main.help
      )
    );

    if (subCmd && menus[subCmd] && menus[subCmd].args) {
      console.log();
      console.log(
        chalk.yellow(
          menus[subCmd].args
        )
      );
    }

    console.log();
  }
}

class Login {
  execute(args) {
    const secret = args.secret || args.s;
    if (secret == null) {
      console.log(
        chalk.red(
          `"-secret" argument is required. Run "hav help login" for details.`
        )
      );
      return;
    }

    let loginUtils = new LoginUtils();
    loginUtils.writeSavedLogin({
      secret: secret
    });
  }
}

class Logout {
  execute(args) {
    let loginUtils = new LoginUtils();
    fs.unlinkSync(loginUtils.getFileName());
    console.log(
      chalk.yellowBright('successfully logged out of Hello VuePress')
    );
  }
}

var version = "0.0.11";

class Version {
  execute(args) {
    console.log(`v${version}`);
  }
}

function index() {
  try {
    const args = minimist(process.argv.slice(2));

    let cmd = args._[0] || 'help';
  
    if (args.version || args.v) {
      cmd = 'version';
    }
  
    if (args.help || args.h) {
      cmd = 'help';
    }
  
    switch (cmd) {
      case 'login':
        let login = new Login();
        login.execute(args);
        break;
  
      case 'logout':
        let logout = new Logout();
        logout.execute(args);
        break;
  
      case 'hello':
        let hello = new Hello();
        hello.execute(args);
        break;
  
      case 'version':
        let version = new Version();
        version.execute(args);
        break;
  
      case 'help':
        let help = new Help();
        help.execute(args);
        break;
  
      default:
        console.log();
        console.log(
          chalk.white.inverse(
            '  hav: command error  '
          )
        );
        console.log(
          chalk.red(
            `     "${cmd}" is not a valid command!`
          )
        );
        console.log(
          chalk.white(
            '     run "hav help" for a list of valid commands'
          )
        );
        console.log();
        break;
    }
  } catch(error) {
    console.log(error);
    log(chalk.redBright('\nerror!'));
    log(chalk.whiteBright.bgRed(` ${error.message} `));
    log(chalk.redBright('details provided above\n'));
  }
}

module.exports = index;
