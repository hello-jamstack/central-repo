import chalk from 'chalk';
import clear from 'clear';
import figlet from 'figlet';

const menus = {
  main: {
    command: 'hav [command] <options>',
    help: `
    The Hello VuePress CLI performs the following operations
    against the Hello VuePress online API.

      login .............. logs the user into Hello VuePress
                           with an option to save the login

      logout ............. logs the user out of Hello VuePress

      hello ............... shows the secret hello message

      version ............ show Hello VuePress command line version

      help ............... show help menu for a command

    Options available for all commands:

      --quiet, -q ..... just print the output with no interactive status messages

      --pretty, -p ..... print output in pretty format`,

    args: ' '
    },

  login: {
    command: `hav login [args]`,
    help:`
    logs in the user to Hello VuePress`,
    args: `
      --secret, -s ............ required. shared login secret, this rotates
                                often and can be obtained from your
                                Hello VuePress administrator`
                                
  },

  logout: {
    command: 'hav logout',
    help:`
    logs out the user from Hello VuePress, removing any
    saved login`,
  },

  hello: {
    command: 'hav hello',
    help:`
    shows the secret hello message`,
    args: `
      --config-url, -c ........ required. url that links to the config JSON file
                                for the Hello JAMstack service.  Your
                                administrator will give you this.

      --secret, -s ............ optional. shared login secret, this rotates
                                often and can be obtained from your
                                Hello VuePress administrator. You can optionally
                                use "hav login" to store the secret for use with
                                multiple commands.`
  }

}

export default class Help {

  execute(args) {
    const subCmd = args._[0] === 'help'
      ? args._[1]
      : args._[0]

    clear();
    console.log(
      chalk.cyan(
        figlet.textSync('Hello VuePress CLI', { font: 'standard', horizontalLayout: 'default' })
      )
    );

    if (subCmd && !menus[subCmd]) {
      console.log(
        chalk.red(
          `no help for command "${subCmd}"`
        )
      );
      console.log();
    }

    console.log(
      chalk.whiteBright(
        menus[subCmd] ? menus[subCmd].command : menus.main.command
      )
    );

    console.log(
      chalk.yellowBright(
        menus[subCmd] ? menus[subCmd].help : menus.main.help
      )
    );

    if (subCmd && menus[subCmd] && menus[subCmd].args) {
      console.log();
      console.log(
        chalk.yellow(
          menus[subCmd].args
        )
      );
    }

    console.log();
  }
}
