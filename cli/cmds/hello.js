import { HelloJAMstack } from 'hello-jamstack-sdk';
import ora from 'ora';
import LoginUtils from './login-utils';
import chalk from 'chalk';

const log = console.log;

export default class Hello {

  async execute(args) {
    const quiet = args.quiet || args.q;

    let spinner = null;
    if (!quiet) spinner = ora('starting fetch secret message process', {spinner:'simpleDotsScrolling'}).start();
  
    var secret = args.secret || args.s;
    if (secret == null) {
  
      let loginUtils = new LoginUtils();
      let savedLogin = loginUtils.readSavedLogin();
      if (savedLogin != null) {
        secret = savedLogin.secret;
      }
  
      if (secret == null) {
        let errMsg = `"-secret" argument is required or saved login is required. Run "hav help hello" or "have help login" for details.`;
        console.log(
          chalk.red(
            errMsg
          )
        );
        throw(errMsg);
      }
    }

    const configUrl = args["config-url"] || args.c;
    if (configUrl == null) {
      let errMsg = `"--config-url" argument is required. Run "hav help hello" for details.`
      console.log(
        chalk.red(
          errMsg
        )
      );
      throw(errMsg);
    }
      
    let hjs = new HelloJAMstack();
    let message = await hjs.fetchMessageWithSecret(configUrl, secret, (status) => {
      if (!quiet) spinner.text = status;
    });

    if (!quiet) spinner.text = 'secret message fetch process complete';
    if (!quiet) spinner.succeed();
    const pretty = args.pretty || args.p
    pretty ? console.log(JSON.stringify(message, null, ' ')) : console.log(JSON.stringify(message));
    if (!quiet) log(chalk.greenBright('complete'));
      
  }

}
