import chalk from 'chalk';
import LoginUtils from './login-utils';

export default class Login {
  execute(args) {
    const secret = args.secret || args.s;
    if (secret == null) {
      console.log(
        chalk.red(
          `"-secret" argument is required. Run "hav help login" for details.`
        )
      );
      return;
    }

    let loginUtils = new LoginUtils();
    loginUtils.writeSavedLogin({
      secret: secret
    });
  }
}
