import { version } from '../package.json';

export default class Version {
  execute(args) {
    console.log(`v${version}`);
  }
}
