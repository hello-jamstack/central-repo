import chalk from 'chalk';
import fs from 'fs';
import os from 'os';

export default class LoginUtils {

  constructor() {
    this.fname = os.homedir() + '/.hav';
  }

  getFileName() {
    return this.fname;
  }

  readSavedLogin() {
    let savedLogin = null;

    if (fs.existsSync(this.fname)) {
      let data = fs.readFileSync(this.fname, {encoding: 'utf-8'});
      savedLogin = JSON.parse(data);
    }

    return savedLogin;
  }

  writeSavedLogin(savedLogin) {
    fs.writeFileSync(this.fname, JSON.stringify(savedLogin, null, ' ')); 
    fs.chmodSync(this.fname,'0600');
    
    return console.log(
      chalk.yellowBright(
        `Login persisted to ${this.fname}`
      )
    );
  }
}


