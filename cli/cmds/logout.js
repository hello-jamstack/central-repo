import chalk from 'chalk';
import fs from 'fs';
import LoginUtils from './login-utils';

export default class Logout {
  execute(args) {
    let loginUtils = new LoginUtils();
    fs.unlinkSync(loginUtils.getFileName());
    console.log(
      chalk.yellowBright('successfully logged out of Hello VuePress')
    );
  }
}
