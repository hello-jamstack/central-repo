import json from 'rollup-plugin-json';

export default {
  input: 'index.js',
  output: {
    file: 'lib/bundle.js',
    format: 'cjs'
  },
  plugins: [ json() ]
};
