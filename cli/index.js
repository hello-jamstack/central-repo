import minimist from 'minimist';
import chalk from 'chalk';
import { Hello, Help, Login, Logout, Version } from './cmds/index.js';

export default function() {
  try {
    const args = minimist(process.argv.slice(2));

    let cmd = args._[0] || 'help';
  
    if (args.version || args.v) {
      cmd = 'version';
    }
  
    if (args.help || args.h) {
      cmd = 'help';
    }
  
    switch (cmd) {
      case 'login':
        let login = new Login();
        login.execute(args);
        break;
  
      case 'logout':
        let logout = new Logout();
        logout.execute(args);
        break;
  
      case 'hello':
        let hello = new Hello();
        hello.execute(args);
        break;
  
      case 'version':
        let version = new Version();
        version.execute(args);
        break;
  
      case 'help':
        let help = new Help();
        help.execute(args);
        break;
  
      default:
        console.log();
        console.log(
          chalk.white.inverse(
            '  hav: command error  '
          )
        );
        console.log(
          chalk.red(
            `     "${cmd}" is not a valid command!`
          )
        );
        console.log(
          chalk.white(
            '     run "hav help" for a list of valid commands'
          )
        );
        console.log();
        break;
    }
  } catch(error) {
    console.log(error);
    log(chalk.redBright('\nerror!'));
    log(chalk.whiteBright.bgRed(` ${error.message} `));
    log(chalk.redBright('details provided above\n'));
  }
}
