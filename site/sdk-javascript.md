---
sidebar: auto
---

# Javascript SDK

## Installation

The SDK is in the [npm repository](https://www.npmjs.com/package/hello-jamstack-sdk)

Using npm run...

`npm i hello-jamstack-sdk`

Using Yarn run...

`yarn add hello-jam-stack`

## Using the HelloJAMStack class


All you need to do is import the class and instantiate it, then fetch the message with it.

If you are using ES6...

```javascript
import { HelloJAMstack } from 'hello-jamstack-sdk';

...

let hjs = new HelloJAMstack();
```

If you are using CommonJS...

```javascript
const HelloJaMstack = require('hello-jamstack-sdk').HelloJAMstack;

...

let hjs = new HelloJAMstack();
```

## fetchMessageWithSecret

```javascript
fetchMessageWithSecret(configUri, secret, statusCallback)
```

Fetch the secret message with a shared authentication secret.
Use this for process to process communication, like when
implementing a CLI or similar.

* `configUri` string - url that links to the config JSON file for the Hello
JAMstack service.  Your administrator will give you this.
* `secret` string - shared login secret, this rotates often and can be
obtained from your administrator.
* `statusCallback` function - function that will be called with period
status messages, this function should support a single status string argument

*see also*

* [shared secret](api.md#token-from-secret "shared secret")

*example*

```javascript
// assumes you have the configUrl and secret
let hjs = new HelloJAMstack();
let message = await hjs.fetchMessageWithSecret(configUrl, secret, (status) => {
  console.log(status);
});
console.log(message);
```

## fetchMessageWithToken

```javascript
fetchMessageWithToken(configUri, author, statusCallback)
```

Fetch the secret message with a bearer token from a user login.
Use this for supporting user interfaces, like a web or mobile app.

* `configUri` string - url that links to the config JSON file for the Hello
JAMstack service.  Your administrator will give you this.
* `author` string - OAuth2 bearer token.
* `statusCallback` function - function that will be called with period
status messages, this function should support a single status string argument

*see also*

* [bearer token](api.md#response-2 "bearer token")

*example*

```javascript
// assumes you have the configUrl and secret
let hjs = new HelloJAMstack();
let message = await hjs.fetchMessageWithToken(configUrl, bearerToken, (status) => {
  console.log(status);
});
console.log(message);
```
