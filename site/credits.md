# Credits

* [VuePress](https://vuepress.vuejs.org/) and [Evan You](https://www.patreon.com/evanyou)
* [Microsoft Azure](https://azure.microsoft.com/en-us/)
* [Microsoft Azure DevOps](http://devops.azure.com)
* [Intuitive font family](https://fontlibrary.org/en/font/intuitive) from [Bruno de Souza Leão](https://fontlibrary.org/en/member/kylesatori)
* [Hashicorp Terraform](https://www.terraform.io/)
* [Van Wilson](https://github.com/vjwilson) for his [starter template](https://github.com/vjwilson/enzyme-example-jest) for the Javascript SDK
* [Eagle.js](https://zulko.github.io/eaglejs-demo/#/) for the tech behind the [overview](/overview.md)