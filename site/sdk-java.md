---
sidebar: auto
---

# Java SDK

## Installation

The SDK is in the public Maven repository.

### Gradle

```groovy
dependencies {
    // This dependency is exported to consumers, that is to say found on their compile classpath.
    ...
    api 'com.flacito.jamstack:hello-jamstack-sdk:0.1.0'
    // These dependencies are required for the Hello JAMstack SDK
    api 'com.mashape.unirest:unirest-java:1.4.9'
    api 'org.apache.httpcomponents:httpclient:4.3.6'
    api 'org.apache.httpcomponents:httpasyncclient:4.0.2'
    api 'org.apache.httpcomponents:httpmime:4.3.6'
    api 'org.json:json:20140107'
    ...
}
```

Then run

```
gradlew build
```

### Maven

```xml
<dependency>
  <groupId>com.flacito.jamstack</groupId>
  <artifactId>hello-jamstack-sdk</artifactId>
  <version>0.1.0</version>
</dependency>
```

Then run

```
mvn build
```

## Using the HelloJAMstack class

All you need to do is import the class and instantiate it, then fetch the message with it.

```java
import com.flacito.jamstack.HelloJAMstack;

public class MyClass {

    public static void main(String args[]) {
      try {
        HelloJAMstack hjs = new HelloJAMstack();
        hjs.fetchMessageWithSecret("https://yourserver/config.json","yoursecret");
      } catch (Throwable e) {
        e.printStackTrace();
      }
    }
}
```

 *See also* 
 
 [hellojamstack](https://github.com/flacito/hellojamstack/sdk/java) 

## fetchMessageWithSecret

```Java
public String fetchMessageWithSecret(String configUrl, 
                                     String secret, 
                                     HelloJAMstackListener listener)
```

Fetch the secret message with a shared authentication secret. Use this for process to process communication, like when implementing a CLI or similar.

 * **Parameters:**
   * `configUri` - url that links to the config JSON file for the Hello
     JAMstack service.  Your administrator will give you this.

   * `secret` - shared login secret, this rotates often and can be
     obtained from your administrator.

   * `listener` - listener object that implements the HelloJAMstackListener interface, receives status messages

```java
import com.flacito.jamstack.HelloJAMstack;

public class MyClass {

    public static void main(String args[]) {
      try {
        HelloJAMstack hjs = new HelloJAMstack();
        hjs.fetchMessageWithSecret("https://yourserver/config.json","yoursecret");
      } catch (Throwable e) {
        e.printStackTrace();
      }
    }
}
```

 *See also* 
 
 [shared secret](api.md#token-from-secret)

## fetchMessageWithToken

```java
public String fetchMessageWithToken(String configUrl, 
                                    String token, 
                                    HelloJAMstackListener listener)
```

Fetch the secret message with a bearer token from a user login. Use this for supporting user interfaces, like a web or mobile app.

 * **Parameters:**
   * `configUri` - url that links to the config JSON file for the Hello
     JAMstack service.  Your administrator will give you this.

   * `bearerToken` - OAuth2 bearer token.

   * `listener` - listener object that implements the HelloJAMstackListener interface, receives status messages
     status messages, this function should support a single status string argument
 
 ```java
import com.flacito.jamstack.HelloJAMstack;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.http.JsonNode;

public class MyClass {

    public static void main(String args[]) {
      try {
        String subscriptionID = "yourtenantid";
        String clientID = "yourappid";
        String secret = "yoursecretkeyfortheapp";

        HttpResponse<JsonNode>jsonResponse = Unirest.post("https://login.microsoftonline.com/" + subscriptionId + "/oauth2/token")
          .header("content-type", "application/x-www-form-urlencoded")
          .header("accept", "application/json")
          .body("grant_type=client_credentials&client_id=" + clientId + "&client_secret=" + secret).asJson();

        String token = jsonResponse.getBody().getObject().getString("access_token");

        HelloJAMstack hjs = new HelloJAMstack();
        hjs.fetchMessageWithToken("https://yourserver/config.json",token);
      } catch (Throwable e) {
        e.printStackTrace();
      }
    }
}
```

 *See also* 
 
 [bearer token](api.md#response-2)