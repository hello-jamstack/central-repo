# It's Alive!

Below is a **live call to our backend API**. It demonstrates how a mostly static site is able to still become 
lively and perform as a real Web application if you add a bit of front-end Javascript to it. 

<SeeSecret/>

## The Lesson for everyone

Most of this site is generated from Markdown files that anyone can write. However, this page has a 
[VueJS component](https://gitlab.com/hello-jamstack/central-repo/blob/master/site/.vuepress/components/SeeSecret.vue) in it that makes this page dynamic. Put another way,
you only need big help from a small part of the team that knows Javascript and the Javascript part of the team
just needs to know how to call APIs. If you need your own API you just need another big help from another small 
part of the team that knows how to implement an API.

## If you are a Developer

If you want to know how to develop something like this and know a lot about Java, Javascript, or .NET, take a 
look at the Developer menu on the upper right of this page. You can also jump in and see how the [API](/ api.md) 
for this site works or try out the API in an easy way using our [command-line tool](/cli.md).

## If you are an Architect

If you want to know how to build this _whole thing_ and know about things like Terraform and
using a cloud like Azure, see how we did it in the [architecture](architecture.md) section.
