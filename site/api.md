---
sidebar: auto
---

# Application Programming Interface (API)

This describes the resources that make up the official Hello JAMstack HTTP API. If you have any problems or requests please contact [support](mailto:btwebb@gmail.com). 

____ 

## Get the hello message

Returns the secret message object to show authorized users.  Note, you must have an OAuth bearer token for an authorized user or have the shared secret key
which you can obtain from your administrator.  You will need to allow for frequent rotation of the shared secret because our administration staff rotates it often.

You can get the token for the /hello API call through either a [shared secret call](#token-from-secret) or [a user login](#token-from-user-login).

### Request

```
GET /api/v1/hello

HEADERS
accept: application/json
authorization: Bearer theoauthbearer.token.fromthesystem
```

### Response

#### Status: 200 OK

example

```json
{ "message": "this will be the secret emssage from the Hello JAMstack system" }  
```

## Token from secret

You can get the bearer token with an OAuth2 token call using the following request format.

Use this approach when you are doing a non-user based system to system call, like with
implementing a CLI.

### Request

```
GET https://login.microsoftonline.com/{subscriptionId}/oauth2/token

BODY
grant_type=client_credentials&client_id={clientId}&client_secret={secret}

HEADERS
accept: application/json
content-type: application/x-www-form-urlencoded
```

*where*

`subscriptionId` - Azure subscription ID that has the Azure AD Application Principal with the secret

`clientId` - the Azure Application Principal application ID

`secret` - the secret key created for the Azure AD Application principal

### Response

#### Status: 200 OK

example

```json
{ 
  ...
  "access_token": "bigolelongtokenthatisthe.bearertoken.thatyoucanusetocallthehelloapi" 
  ...
}  
```

The `acces_token` value can be used as the bearer token for the /hello message call.

## Token from user login

You can use [adal.js](https://github.com/AzureAD/azure-activedirectory-library-for-js) and after the user has been prompted to login and successfully logs in, you can
call into the authentication context for the token.

Use this approach when a user can login, like in a Web page or mobile application.

For example,

```javascript
  acquireToken() {
    return new Promise((resolve, reject) => {

      if (!this.clientId) {
        console.log('error: no client ID, please provide a valid Azure AD application ID')
      }
      else {
        this.authenticationContext.acquireToken(this.clientId, (error, token) => {
          if (error || !token) {
            return reject(error);
          } else {
            return resolve(token);
          }
        });
      }
    });
  },

```

The `token` value required by acquireToken can then be used as the bearer token for the /hello message call.