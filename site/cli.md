---
sidebar: auto
---

# Command Line Interface (CLI)

## Installation

1. On Windows open a Git Bash window (you can install Git Bash from [here](http://git-scm.com)). 
  On Mac and Linux users can just use a terminal.
1. Run `curl -fL http://localhost:8080/install-cli.sh | bash` in the command window

## Hello

Fetches the secret hello message from the system.  If you get an error, did you remember to use the secret or save your [login](#login)?

If you do not prefer to save your login...

`hav hello --config-url https://yourserver/config-hello-jamstack.json --secret yoursecret` 

If you have a [saved login](#login)...

`hav hello --config-url https://yourserver/config-hello-jamstack.json ` 

*where*

`--config-url, -c` -  required. url that links to the config JSON file for the Hello JAMstack service.  Your administrator will give you this.

`--secret, -s` - optional. shared login secret, this rotates often and can be obtained from your administrator. You can optionally use "hav login" to store the secret for use with multiple commands.

*example*

```bash
> hav hello -c https://hellovuepress-webapp.azurewebsites.net/config-hello-jamstack.json
✔ secret message fetch process complete
{"message":"The secret message that you get from Hello JAMstack will be here"}
complete
```

## Login

`hav login -s thesecret`

Logs you into the system, where "thesecret" should be changed to the actual secret your
administrator gives you. You will need a secret from your adminstrator.  Good adminstrators 
rotate secrets often, so be ready to login regularly.

*where*

`--secret, -s` - optional. shared login secret, this rotates often and can be obtained from your administrator. You can optionally use "hav login" to store the secret for use with multiple commands.

*example*

```bash
> hav login -s supersecretkeyfromyouradmin
Login persisted to /Users/userhome/.hav
```

## Logout

`hav logout`

Logs you out of the system, removing any saved login.

*example*

```bash
> hav logout
successfully logged out of Hello JAMstack
```

## Version

`hav version` or `hav -v` or `hav --version`

Show the version of the CLI.

*example*

```bash
> hav version
v0.0.10
```

## Help

`hav help`

Shows the main help that lists all the available commands.

`hav help command`

Shows the help for the CLI command, where "command" is a command available for the CLI. 

*example*

```
> hav help
  _   _      _ _        __     __          ____                       ____ _     ___
 | | | | ___| | | ___   \ \   / /   _  ___|  _ \ _ __ ___  ___ ___   / ___| |   |_ _|
 | |_| |/ _ \ | |/ _ \   \ \ / / | | |/ _ \ |_) | '__/ _ \/ __/ __| | |   | |    | |
 |  _  |  __/ | | (_) |   \ V /| |_| |  __/  __/| | |  __/\__ \__ \ | |___| |___ | |
 |_| |_|\___|_|_|\___/     \_/  \__,_|\___|_|   |_|  \___||___/___/  \____|_____|___|

hav [command] <options>

    The Hello VuePress CLI performs the following operations
    against the Hello VuePress online API.

      login .............. logs the user into Hello VuePress
                           with an option to save the login

      logout ............. logs the user out of Hello VuePress

      hello ............... shows the secret hello message

      version ............ show Hello VuePress command line version

      help ............... show help menu for a command

    Options available for all commands:

      --quiet, -q ..... just print the output with no interactive status messages

      --pretty, -p ..... print output in pretty format
```

