---
sidebar: auto
---

# .NET SDK

## Installation

The SDK is in the public NuGet repository.

Change to the directory of the project in which you want to use the Hello JAMstack SDK and do one of the following.

### Powershell
```powershell
Install-Package hello-jamstack-sdk -Version 1.0.0
```

### .NET CLI
```bash
dotnet add package hello-jamstack-sdk --version 1.0.0
```

### Packet CLI
```bash
paket add hello-jamstack-sdk --version 1.0.0
```


## Using the HelloJAMstack class

All you need to do is import the class and instantiate it, then fetch the message with it.

```java
using HelloJAMstack;

namespace MyNamespace
{
    public class MyClass
    {
        public async void fetchMessage()
        {
            HelloJAMstack hjs = new HelloJAMstack();
            var secret = System.Environment.GetEnvironmentVariable("HELLO_JAMSTACK_SECRET");
            if (secret == null) {
              throw new Exception("Please set your environment variable HELLO_JAMSTACK_SECRET with your Hello JAMstack secret your admin gave you");
              
            }
            var message = await hjs.fetchMessageWithSecret("https://hellovuepress-webapp.azurewebsites.net/config-hello-jamstack.json",secret);
            Console.WriteLine(message);
        }
    }
}
```

## fetchMessageWithSecret

```java
public async Task<string> fetchMessageWithSecret(string configUrl, string secret) 
```

Fetch the secret message with a shared authentication secret. Use this for process to process communication, like when implementing a CLI or similar.

 * **Parameters:**
   * `configUrl` - url that links to the config JSON file for the Hello
     JAMstack service.  Your administrator will give you this.

   * `secret` - shared login secret, this rotates often and can be
     obtained from your administrator.

```java
HelloJAMstack hjs = new HelloJAMstack();
var secret = System.Environment.GetEnvironmentVariable("HELLO_JAMSTACK_SECRET");
if (secret == null) {
  throw new Exception("Please set your environment variable HELLO_JAMSTACK_SECRET with your Hello JAMstack secret your admin gave you");
}
var message = await hjs.fetchMessageWithSecret("https://hellovuepress-webapp.azurewebsites.net/config-hello-jamstack.json",secret);
Console.WriteLine(message);
```

## fetchMessageWithToken

```java
public async Task<string> fetchMessageWithToken(string configUrl, string token) 
```

Fetch the secret message with a bearer token from a user login. Use this for supporting user interfaces, like a web or mobile app.

 * **Parameters:**
   * `configUrl` - url that links to the config JSON file for the Hello
     JAMstack service.  Your administrator will give you this.

   * `token` - OAuth2 bearer token.
 
 ```java
var tenant = System.Environment.GetEnvironmentVariable("HELLO_JAMSTACK_TENANT");
if (tenant == null) {
  throw new Exception("Please set your environment variable HELLO_JAMSTACK_TENANT with your Azure subscription ID");
}

var clientId = System.Environment.GetEnvironmentVariable("HELLO_JAMSTACK_CLIENT_ID");
if (clientId == null) {
  throw new Exception("Please set your environment variable HELLO_JAMSTACK_CLIENT_ID with your application ID for your Azure AD service principal");
}

var secret = System.Environment.GetEnvironmentVariable("HELLO_JAMSTACK_SECRET");
if (secret == null) {
  throw new Exception("Please set your environment variable HELLO_JAMSTACK_SECRET with your Hello JAMstack secret your admin gave you");
  
}

var TokenResponse = await "https://login.microsoftonline.com/"
    .AppendPathSegment(String.Format("{0}/oauth2/token", tenant))
    .WithHeader("Accept", "application/json")
    .WithHeader("content-type", "application/x-www-form-urlencoded")
    .PostStringAsync(String.Format("grant_type=client_credentials&client_id={0}&client_secret={1}", clientId, secret))
    .ReceiveJson<TokenResponse>();

var message = await this.fetchMessageWithToken(configUrl,TokenResponse.access_token);
```
