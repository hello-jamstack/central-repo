---
sidebar: auto
---

# Architecture

## VueJS

This site is served as a [Single Page Application](https://en.wikipedia.org/wiki/Single-page_application)
that was compiled by [VuePress](https://vuepress.vuejs.org/) into a [VueJS](https://vuejs.org/) web application.
Most of the application is a bunch of [Markdown](https://daringfireball.net/projects/markdown/) files but there
is a custom [VueJS component](https://vuepress.vuejs.org/guide/using-vue.html#using-components) that fetches
the message for you. This app, which is just static HTML and Javascript, is uploaded to [Azure We Apps](https://azure.microsoft.com/en-us/services/app-service/web/) and can be served at global scale if funded.

## Azure

How do you get the message?  First you have to login using a valid [Azure AD ID](https://azure.microsoft.com/en-us/services/active-directory/)
because the Javascript in the VueJS application protects the message until OAuth is used to sign into Azure AD.
Once logged in, the Javascript in the VueJS application calls into [Azure Functions](https://azure.microsoft.com/en-us/services/functions/)
with your sign-in credentials and retrieves the message. 

## Terraform

The entire system is stored as code in [Azure DevOps](https://dev.azure.com/flacito/hello-azure-app-service/_git/hello-azure-vuepress)
and delivered through an automated pipeline to Azure using [the Azure ARM Provider for Terraform](https://www.terraform.io/docs/providers/azurerm/index.html).  It's 99%* deployed through
a Terraform run.

## System Diagram

Here's a picture of the system:

![Hello VuePress architecture](/architecture.svg)

## Notes

\* we had to back off deploying the Azure AD app registration, we do that by hand. Also, where the Azure Terraform provider
lacked functionality we used the [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/get-started-with-azure-cli?view=azure-cli-latest). 
We expect to fully automate this as the Azure Terraform provider matures

\** we initially had the message stored in Azure CosmosDB behind the Auzure function, but it ate too much of our monthly MSDN allowance.