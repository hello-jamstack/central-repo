module.exports = {
    head: [
        ['link', { rel: 'icon', href: '/icon.png' }]
    ],
    dest: '.build',
    title: 'Hello',
    description: 'this is a JAMstack site',
    themeConfig: {
        logo: '/logo.svg',
        nav: [
            {
              text: 'About',
              items: [
                { text: 'Overview', link: 'overview.md' },
                { text: 'JAMstack', link: 'jamstack.md' },
                { text: 'Architecture', link: 'architecture.md' },
                { text: 'T\'s & C\'s', link: 'terms.md' },
                { text: 'Privacy', link: 'privacy.md' },
                { text: 'Credits', link: 'credits.md' },
              ]
            },
            {
              text: 'Developer',
              items: [
                { text: 'API', link: 'api.md' },
                { text: 'CLI', link: 'cli.md' },
                { text: 'Javascript SDK', link: 'sdk-javascript.md' },
                { text: 'Java SDK', link: 'sdk-java.md' },
                { text: '.NET SDK', link: 'sdk-dotnet.md' },
              ]
            },
            { text: "It's Alive", link: 'seesecret.md' },
          ]
    }
}