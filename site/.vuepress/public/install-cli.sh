#!/bin/bash
echo 'Checking prequisites'
command -v node >/dev/null 2>&1 || { echo >&2 "Node.js is required but it is not installed on your system.  Please see https://nodejs.org/en/download/package-manager/ to install Node.js."; exit 1; }
echo 'Installing CLI'
npm i -g hello-azure-vuepress-cli
echo 'Verifying CLI'
command -v hav >/dev/null 2>&1 || { echo >&2 "Installation failed.  Please see https://hellovuepress-webapp.azurewebsites.net/ for support."; exit 1; }
echo 'Success!'
hav help
