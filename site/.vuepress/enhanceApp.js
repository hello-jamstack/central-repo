import authentication from './js/authentication';
import api from './js/api';

export default ({
    Vue, // the version of Vue being used in the VuePress app
    options, // the options for the root Vue instance
    router, // the router instance for the app
    siteData // site metadata
  }) => {

    Vue.mixin({
      computed: {
        $auth() {
            return authentication;
        },
        $api() {
          return api;
      }
    }
    })    
  }