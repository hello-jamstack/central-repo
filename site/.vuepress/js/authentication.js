import AuthenticationContext from 'adal-angular/lib/adal.js';
const axios = require('axios');

export default {
  authenticationContext: null,
  clientId: null,

  /**
   * @return {Promise}
   */
  initialize() {

    return new Promise((resolve, reject) => {
      axios
      .get('/config-hello-jamstack.json')
      .then(response => {
        let cfg = response.data;
        // console.log('TCL: initialize -> cfg', JSON.stringify(cfg, null, ' '));
        this.clientId = cfg.clientId;
        this.authenticationContext = new AuthenticationContext(cfg);
    
        if (!this.isAuthenticated() && this.authenticationContext.isCallback(window.location.hash) || window.self !== window.top) {
          // redirect to the location specified in the url params.
          this.authenticationContext.handleWindowCallback();
        }
        else {
          // try pull the user out of local storage
          let user = this.authenticationContext.getCachedUser();
  
          if (user) {
            // console.log('TCL: initialize -> user: ', JSON.stringify(user, null, ' '));
            resolve(user);
          }
          else {
            console.log('TCL: initialize -> no user yet.');
          }
        }
      })
      .catch(error => {
        console.log(error)
      });
    });
  },

  /**
   * @return {Promise.<String>} A promise that resolves to an ADAL token for resource access
   */
  acquireToken() {
    return new Promise((resolve, reject) => {

      if (!this.clientId) {
        console.log('error: no client ID, please provide a valid Azure AD application ID')
      }
      else {
        this.authenticationContext.acquireToken(this.clientId, (error, token) => {
          if (error || !token) {
            return reject(error);
          } else {
            return resolve(token);
          }
        });
      }
    });
  },

  /**
   * Issue an interactive authentication request for the current user and the api resource.
   */
  acquireTokenRedirect() {
    this.authenticationContext.acquireTokenRedirect('<azure active directory resource id>');
  },

  /**
   * @return {Boolean} Indicates if there is a valid, non-expired access token present in localStorage.
   */

  isAuthenticated() {
    // getCachedToken will only return a valid, non-expired token.
    // console.log('TCL: initialize -> this.authenticationContext', JSON.stringify(this.authenticationContext, null, ' '));
    if (this.authenticationContext.getCachedToken(this.clientId)) { return true; }
    return false;
  },

  getName() {
    let rval = null;
    if (this.authenticationContext && this.authenticationContext._user && this.authenticationContext._user.profile) {
      rval = this.authenticationContext._user.profile.name;
    }
    return rval;
  },

  /**
   * @return An ADAL user profile object.
   */
  getUserProfile() {
    return this.authenticationContext.getCachedUser().profile;
  },

  signIn() {
    console.log('TCL: signIn -> signIn');
    this.authenticationContext.login();
  },

  signOut() {
    this.authenticationContext.logOut();
  }

}
