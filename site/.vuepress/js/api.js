const axios = require('axios');
const HelloJAMStack = require('hello-jamstack-sdk').HelloJAMstack;

export default {

  fetchSecretMessage(bearerToken) {
    return new Promise((resolve) => {
      let helloJAMStack = new HelloJAMStack();
      helloJAMStack.fetchMessageWithToken('/config-hello-jamstack.json', bearerToken, (status) => {
        console.log(status);
      }).then(messageJson => {
        resolve(messageJson.message);
      });
    });
  }

}