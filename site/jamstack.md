---
pageClass: about-page
---

# JAMstack

This site may seem simple, but it isn't.  It's an attempt to build a [JAMstack](https://jamstack.org/). 

## JAMstack

### J

through the Javascript-based static site generator, [VuePress](https://vuepress.vuejs.org/), we get the "J" in JAM. 

### A

through [Azure Functions](https://azure.microsoft.com/en-us/services/functions/) we an API to add dynamic functionality to the app, we get the "A" in JAM.

### M

through VuePress' support of [Markdown](https://daringfireball.net/projects/markdown/) we get the "M" in JAM.  While there are some custom [VueJS components](https://vuepress.vuejs.org/guide/using-vue.html#using-componentsl) in the site, the majority of the content is written in Markdown, which means the majority of the content didn't need a front-end developer to get it there.

## Notables

1. [Azure DevOps](https://dev.azure.com/flacito/hello-azure-app-service/_git/hello-azure-vuepress) to store and deliver the code.
1. [Azure Web Apps](https://azure.microsoft.com/en-us/services/app-service/web/) to host this site, which is just a statically rendered [Single Page Application](https://en.wikipedia.org/wiki/Single-page_application). No app servers required!
1. [Azure Active Directory](https://docs.microsoft.com/en-us/azure/active-directory/manage-apps/what-is-application-management) and the [Azure Active Directory JavaScript library](https://github.com/AzureAD/azure-activedirectory-library-for-js) to protect secrets on this site.
1. [Azure Functions](https://azure.microsoft.com/en-us/services/functions/) for the API.

See details in the [architecture](architecture.md) section.