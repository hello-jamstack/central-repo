provider "azurerm" {
  subscription_id = "${var.subscription_id}"
  client_id = "${var.client_id}"
  client_secret = "${var.client_secret}"
  tenant_id = "${var.tenant_id}"
}

resource "null_resource" "clean" {

  provisioner "local-exec" {
    command = <<EOF
      set -e
      export REPO_DIR="${path.module}/../.."
      export BUILD_DIR="$${REPO_DIR}/.build"
      rm -Rf "$${BUILD_DIR}"
      echo 'cleanup complete'
EOF
    }
}

resource "azurerm_resource_group" "hellovuepress_resource_group" {
  name = "${var.deployment_name}"
  location = "${var.location}"
}

# resource "azurerm_azuread_service_principal" "hello_vuepress_service_principal" {
#   application_id = "${azurerm_azuread_application.hellovuepress_azuread_application.application_id}"
# }

# output "service_prinicpal_object_id" {
#   value = "${azurerm_azuread_service_principal.hello_vuepress_service_principal.id}"
# }

# resource "azurerm_azuread_application" "hellovuepress_azuread_application" {
#   name = "${azurerm_resource_group.hellovuepress_resource_group.name}"
#   homepage = "https://${azurerm_app_service.hellovuepress_webapp_app_service.default_site_hostname}"
#   reply_urls = [
#     "https://${azurerm_app_service.hellovuepress_webapp_app_service.default_site_hostname}/seesecret.html"
#   ]
#   available_to_other_tenants = false
#   oauth2_allow_implicit_flow = false
# }

data "azurerm_azuread_application" "hellovuepress_azuread_application" {
  object_id = "${var.ad_application_object_id}"
}
output "applicaiton_id" {
  value = "${data.azurerm_azuread_application.hellovuepress_azuread_application.application_id}"
}

