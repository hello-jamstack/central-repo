# resource "azurerm_key_vault" "hello_vuepress_keyvault" {
#   name = "${replace(azurerm_resource_group.hellovuepress_resource_group.name, "/_/", "x")}kv"
#   resource_group_name = "${azurerm_resource_group.hellovuepress_resource_group.name}"
#   location = "${azurerm_resource_group.hellovuepress_resource_group.location}"
#   tenant_id = "${var.tenant_id}"

#   sku {
#     name = "standard"
#   }

#   access_policy {
#     tenant_id = "${var.tenant_id}"
#     application_id  = "${var.ad_application_id}"
#     object_id  = "${var.ad_application_object_id}"

#     key_permissions = []

#     secret_permissions = [
#       "get"
#     ]
#   }

# }

# resource "azurerm_key_vault_access_policy" "admin_keyvault_access" {
#   vault_name           = "${azurerm_key_vault.hello_vuepress_keyvault.name}"
#   resource_group_name  = "${azurerm_key_vault.hello_vuepress_keyvault.resource_group_name}"

#   tenant_id = "${var.tenant_id}"
#   object_id = "${var.admin_user_id}"

#   key_permissions = []

#   secret_permissions = [
#     "get", "set", "list"
#   ]

# }
# resource "null_resource" "configure_functions_key" {

#   provisioner "local-exec" {
#     command = <<EOF
#       set -e
#       USER_NAME=$(az webapp deployment list-publishing-profiles -n ${azurerm_function_app.hellovuepress_function_app.name} -g ${azurerm_resource_group.hellovuepress_resource_group.name} --query "[?publishMethod=='MSDeploy'].userName" --output tsv)
#       USER_PASSWORD=$(az webapp deployment list-publishing-profiles -n ${azurerm_function_app.hellovuepress_function_app.name} -g ${azurerm_resource_group.hellovuepress_resource_group.name} --query "[?publishMethod=='MSDeploy'].userPWD" --output tsv)
#       BEARER_TOKEN=$(curl -u $${USER_NAME}:$${USER_PASSWORD} -sb -X GET https://${azurerm_function_app.hellovuepress_function_app.name}.scm.azurewebsites.net/api/functions/admin/token | sed 's/.\(.*\)/\1/' | sed 's/\(.*\)./\1/')
#       MASTER_CODE=$(curl -sb -X GET -H "Authorization: Bearer $${BEARER_TOKEN}" https://${azurerm_function_app.hellovuepress_function_app.name}.azurewebsites.net/admin/host/systemkeys/_master | jq '.value' | sed 's/.\(.*\)/\1/' | sed 's/\(.*\)./\1/')
#       echo $MASTER_CODE
#       az keyvault secret set --name "api-code" --vault-name ${azurerm_key_vault.hello_vuepress_keyvault.name} --value "$${MASTER_CODE}"
# EOF
#   }
# }
