resource "azurerm_app_service_plan" "hellovuepress_webapp_app_service_plan" {
  name = "${azurerm_resource_group.hellovuepress_resource_group.name}_webapp_app_service_plan"
  resource_group_name = "${azurerm_resource_group.hellovuepress_resource_group.name}"
  location = "${azurerm_resource_group.hellovuepress_resource_group.location}"

  sku {
    tier = "Standard"
    size = "S1"
  }
}

resource "azurerm_app_service" "hellovuepress_webapp_app_service" {
  name = "${replace(azurerm_resource_group.hellovuepress_resource_group.name, "/_/", "-")}-webapp"
  resource_group_name = "${azurerm_resource_group.hellovuepress_resource_group.name}"
  location = "${azurerm_resource_group.hellovuepress_resource_group.location}"
  app_service_plan_id = "${azurerm_app_service_plan.hellovuepress_webapp_app_service_plan.id}"

  site_config {
    dotnet_framework_version = "v4.0"
    scm_type = "LocalGit"
    default_documents = ["index.html"]
  }
}

# resource "azurerm_app_service_custom_hostname_binding" "hellovuepress_webapp_hostname_binding" {
#   hostname = "about.shrinksr.us"
#   app_service_name = "${azurerm_app_service.hellovuepress_webapp_app_service.name}"
#   resource_group_name = "${azurerm_resource_group.hellovuepress_resource_group.name}"
# }

resource "null_resource" "prep_webapp" {

  provisioner "local-exec" {
    command = <<EOF
      set -e
      export REPO_DIR="${path.module}/../.."
      export BUILD_DIR="$${REPO_DIR}/.build"
      mkdir -p "$${BUILD_DIR}/sitesrc"
      cp -R  "$${REPO_DIR}/site/" "$${BUILD_DIR}/sitesrc"
      echo '{ "tenant": "${var.tenant_id}", "clientId": "${data.azurerm_azuread_application.hellovuepress_azuread_application.application_id}", "cacheLocation": "localStorage", "redirectUri":"https://${azurerm_app_service.hellovuepress_webapp_app_service.name}.azurewebsites.net/seesecret.html", "apiEndpoint": "https://${azurerm_function_app.hellovuepress_function_app.name}.azurewebsites.net/api" }' > "$${BUILD_DIR}/sitesrc/.vuepress/public/config-hello-jamstack.json"
      sed -i -e 's/http/https/g' "$${BUILD_DIR}/sitesrc/cli.md"
      sed -i -e 's/localhost:8080/${azurerm_app_service.hellovuepress_webapp_app_service.name}.azurewebsites.net/g' "$${BUILD_DIR}/sitesrc/cli.md"
      cd "$${BUILD_DIR}/sitesrc" && npm install && npm run site:build
      cp -R "$${BUILD_DIR}/sitesrc/.build/" "$${BUILD_DIR}/site"
      echo 'Site prep complete'
EOF
    }
}

resource "null_resource" "deploy_webapp" {

  depends_on = ["null_resource.prep_webapp"]

  provisioner "local-exec" {
    command = <<EOF
      set -e
      export REPO_DIR="${path.module}/../.."
      export BUILD_DIR="$${REPO_DIR}/.build"
      cd "$${BUILD_DIR}/site" && zip -r ./site.zip ./*
      echo 'Site zip complete'
      az webapp deployment source config-zip -g ${azurerm_resource_group.hellovuepress_resource_group.name} -n ${azurerm_app_service.hellovuepress_webapp_app_service.name}  --src  "$${BUILD_DIR}/site/site.zip"
      echo 'Site deploy complete'
EOF
    }
}
