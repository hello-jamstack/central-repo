resource "azurerm_app_service_plan" "hellovuepress_functions_app_service_plan" {
  name = "${azurerm_resource_group.hellovuepress_resource_group.name}_functions_service_plan"
  resource_group_name = "${azurerm_resource_group.hellovuepress_resource_group.name}"
  location = "${azurerm_resource_group.hellovuepress_resource_group.location}"

  sku {
    tier = "Free"
    size = "F1"
  }
}

resource "azurerm_storage_account" "hellovuepress_storage_account" {
  name = "${replace(azurerm_resource_group.hellovuepress_resource_group.name, "/_/", "x")}fsa"
  resource_group_name = "${azurerm_resource_group.hellovuepress_resource_group.name}"
  location = "${azurerm_resource_group.hellovuepress_resource_group.location}"
  account_tier = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_function_app" "hellovuepress_function_app" {
  name = "${replace(azurerm_resource_group.hellovuepress_resource_group.name, "/_/", "-")}-functions"
  resource_group_name = "${azurerm_resource_group.hellovuepress_resource_group.name}"
  location = "${azurerm_resource_group.hellovuepress_resource_group.location}"
  app_service_plan_id = "${azurerm_app_service_plan.hellovuepress_functions_app_service_plan.id}"
  storage_connection_string = "${azurerm_storage_account.hellovuepress_storage_account.primary_connection_string}"

  # site_config {
  #   always_on = true
  # }
}

# resource "azurerm_app_service_custom_hostname_binding" "hellovuepress_hostname_binding" {
#   hostname = "shrinksr.us"
#   app_service_name = "${azurerm_app_service.hellovuepress_functions_app_service_plan.name}"
#   resource_group_name = "${azurerm_resource_group.hellovuepress_resource_group.name}"
# }

resource "null_resource" "deploy_function_app" {
    provisioner "local-exec" {
      command = <<EOF
        set -e
        export REPO_DIR="${path.module}/../.."
        export BUILD_DIR="$${REPO_DIR}/.build"
        mkdir -p "$${BUILD_DIR}/functions"
        cp -R "$${REPO_DIR}/api/azure-functions/" "$${BUILD_DIR}/functions"
        cd "$${BUILD_DIR}/functions" && zip -r ./functions.zip ./*
        az functionapp deployment source config-zip \
          -g ${azurerm_resource_group.hellovuepress_resource_group.name} -n ${azurerm_function_app.hellovuepress_function_app.name} \
          --src ./functions.zip
EOF
    }
}

resource "null_resource" "add_function_app_auth" {
  provisioner "local-exec" {
    command = <<EOF
      az webapp auth update \
        --resource-group ${azurerm_resource_group.hellovuepress_resource_group.name} \
        --name ${azurerm_function_app.hellovuepress_function_app.name} \
        --aad-client-id ${var.ad_application_id} \
        --action LoginWithAzureActiveDirectory \
        --aad-token-issuer-url https://sts.windows.net/6a8098a5-cc62-4967-b771-3b2d23b332e2/ \
        --allowed-external-redirect-urls https://hellovuepressdev-functions.azurewebsites.net/.auth/login/aad/callback
EOF
  }
}

resource "null_resource" "set_function_app_cors" {

  provisioner "local-exec" {
    command = <<EOF
      set -e
      az webapp cors remove -g ${azurerm_resource_group.hellovuepress_resource_group.name} -n ${azurerm_function_app.hellovuepress_function_app.name} --allowed-origins 'https://functions.azure.com'
      az webapp cors remove -g ${azurerm_resource_group.hellovuepress_resource_group.name} -n ${azurerm_function_app.hellovuepress_function_app.name} --allowed-origins 'https://functions-staging.azure.com'
      az webapp cors remove -g ${azurerm_resource_group.hellovuepress_resource_group.name} -n ${azurerm_function_app.hellovuepress_function_app.name} --allowed-origins 'https://functions-next.azure.com'
      az webapp cors add -g ${azurerm_resource_group.hellovuepress_resource_group.name} -n ${azurerm_function_app.hellovuepress_function_app.name} --allowed-origins '*'
EOF
    }
}


