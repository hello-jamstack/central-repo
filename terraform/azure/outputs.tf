output "web_url" {
  value = "https://${azurerm_app_service.hellovuepress_webapp_app_service.default_site_hostname}"
}