variable "deployment_name" { }
variable "location" { }
variable "failover_location" { }

variable "subscription_id" { }
variable "client_id" { }
variable "client_secret" { }
variable "tenant_id" { }
variable "admin_user_id" { }
variable "ad_application_id" { }
variable "ad_application_object_id" { }
